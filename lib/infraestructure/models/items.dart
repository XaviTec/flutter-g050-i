class Items {
  final String title;
  final String subTitle;
  final String price;
  final String icon;
  final double raiting;
  Items({
    required this.title,
    required this.subTitle,
    required this.price,
    required this.icon,
    required this.raiting,
  });
}

final List<Items> items = [
  Items(
    title: 'Salmon Salad',
    subTitle: 'Baked salmon fish',
    price: '5.50',
    icon: 'assets/img/item1.png',
    raiting: 4.5,
  ),
  Items(
    title: 'Salmon Salad',
    subTitle: 'Baked salmon fish',
    icon: 'assets/img/item2.png',
    price: '8.25',
    raiting: 4.0,
  ),
  Items(
    title: 'Salmon Salad',
    subTitle: 'Baked salmon fish',
    price: '5.50',
    icon: 'assets/img/item1.png',
    raiting: 4.5,
  ),
];

final List<Items> cart = [
  Items(
    title: 'Red n hot pizza',
    subTitle: 'Spicy chicken, beef',
    price: '\$15.30',
    icon: 'assets/img/car1.png',
    raiting: 02,
  ),
  Items(
    title: 'Greek Salad',
    subTitle: 'with baked salmon',
    icon: 'assets/img/car2.png',
    price: '\$12.00',
    raiting: 03,
  ),
];
