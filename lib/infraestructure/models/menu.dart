class Menu {
  final String name;
  final String? icon;
  Menu({
    required this.name,
    this.icon,
  });
}

final List<Menu> menu = [
  Menu(name: 'Burger', icon: 'assets/img/Burger.png'),
  Menu(name: 'Donat', icon: 'assets/img/Donat.png'),
  Menu(name: 'Pizza', icon: 'assets/img/Pizza.png'),
  Menu(name: 'Mexican', icon: 'assets/img/Mexican.png'),
  Menu(name: 'Asian', icon: 'assets/img/Asian.png')
];

final List<Menu> sideMenu = [
  Menu(name: 'My Orders', icon: 'assets/svg/Comunity.svg'),
  Menu(name: 'My Profile', icon: 'assets/svg/Profile.svg'),
  Menu(name: 'Delivery Address', icon: 'assets/svg/Location.svg'),
  Menu(name: 'Payment Methods', icon: 'assets/svg/Wallet.svg'),
  Menu(name: 'Contact Us', icon: 'assets/svg/Message.svg'),
  Menu(name: 'Settings', icon: 'assets/svg/Setting.svg'),
  Menu(name: 'Helps & FAQs', icon: 'assets/svg/Helps.svg')
];
