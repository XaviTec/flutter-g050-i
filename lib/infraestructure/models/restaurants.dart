import 'package:nike_shoes/infraestructure/models/models.dart';

class Restaurants {
  final String name;
  final String icon;
  final String delivery;
  final String deliveryIcon;
  final String time;
  final String timeIcon;
  final List<Menu> menu;
  Restaurants({
    required this.name,
    required this.icon,
    required this.delivery,
    required this.deliveryIcon,
    required this.time,
    required this.timeIcon,
    required this.menu,
  });
}

final List<Restaurants> restaurants = [
  Restaurants(
    name: 'McDonalds',
    icon: 'assets/img/mcdonalds.jpg',
    delivery: 'Free delivery',
    deliveryIcon: 'assets/svg/delivery.svg',
    time: '10-15 mins',
    timeIcon: 'assets/svg/time.svg',
    menu: [
      Menu(name: 'BURGER'),
      Menu(name: 'CHICKEN'),
      Menu(name: 'FAST FOOD'),
    ],
  ),
  Restaurants(
    name: 'Kentucky Fried Chicken',
    icon: 'assets/img/kfc.jpeg',
    delivery: 'Free delivery',
    deliveryIcon: 'assets/svg/delivery.svg',
    time: '15-20 mins',
    timeIcon: 'assets/svg/time.svg',
    menu: [
      Menu(name: 'BIG BOX'),
      Menu(name: 'SNACK BOX'),
      Menu(name: 'KENTUCKY'),
    ],
  ),
  Restaurants(
    name: 'Starbuck',
    icon: 'assets/img/starbucks.jpg',
    delivery: 'Free delivery',
    deliveryIcon: 'assets/svg/delivery.svg',
    time: '25-30 mins',
    timeIcon: 'assets/svg/time.svg',
    menu: [
      Menu(name: 'CARAMEL'),
      Menu(name: 'MOCHA'),
      Menu(name: 'EXPRESSO'),
    ],
  )
];
