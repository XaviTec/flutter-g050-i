class DetalleFactura {
  final String title;
  final String price;
  final String? items;
  DetalleFactura({
    this.items,
    required this.title,
    required this.price,
  });
}

final List<DetalleFactura> detalleFactura = [
  DetalleFactura(title: 'Subtotal', price: '\$27.30'),
  DetalleFactura(title: 'Tax and Fees', price: '\$5.30'),
  DetalleFactura(title: 'Delivery', price: '\$1.00'),
  DetalleFactura(title: 'Total', price: '\$33.60', items: '(2 items)')
];
