import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';

final GlobalKey<SideMenuState> sideMenuKey = GlobalKey<SideMenuState>();

final sideMenuProvider = StateProvider((ref) => sideMenuKey);
