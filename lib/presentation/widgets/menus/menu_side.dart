import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nike_shoes/config/constants/environament.dart';
import 'package:nike_shoes/infraestructure/models/models.dart';

class MenuSide extends StatelessWidget {
  const MenuSide({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            'assets/img/avatar.png',
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Farion Wick',
            style: TextStyle(
              fontSize: 29,
              fontWeight: FontWeight.w900,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          const Text('farionwick@gmail.com'),
          const SizedBox(
            height: 25,
          ),
          Column(
            children: sideMenu
                .map(
                  (item) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          width: 28,
                          item.icon!,
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Text(
                          item.name,
                          style: const TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                )
                .toList(),
          ),
          const SizedBox(
            height: 140,
          ),
          Container(
              width: 117,
              height: 43,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Enviroment.primeryColor,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/img/power.png',
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      'Log Out',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
