import 'package:flutter/material.dart';
import 'package:nike_shoes/config/constants/environament.dart';
import 'package:nike_shoes/infraestructure/models/models.dart';

class Category extends StatelessWidget {
  const Category({
    super.key,
    required this.menu,
  });

  final List<Menu> menu;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: SizedBox(
        height: 125,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: menu.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(9),
              child: Container(
                decoration: BoxDecoration(
                  color: index == 0 ? Enviroment.primeryColor : Colors.white,
                  borderRadius: BorderRadius.circular(40),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 4,
                      offset: Offset(4, 5), //hadow position
                    ),
                  ],
                ),
                width: 65,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                    Image.asset(
                      menu[index].icon!,
                      width: 50,
                      height: 50,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Expanded(
                      child: Text(
                        menu[index].name,
                        style: TextStyle(
                          color: index == 0
                              ? Colors.white
                              : const Color(0xFF67666D),
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
