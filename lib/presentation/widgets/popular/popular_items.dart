import 'package:flutter/material.dart';
import 'package:nike_shoes/config/constants/environament.dart';
import 'package:nike_shoes/infraestructure/models/models.dart';

class PopularItems extends StatelessWidget {
  const PopularItems({super.key, required this.items});
  final List<Items> items;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 236,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: items.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: 155,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 4,
                    offset: Offset(4, 5), //hadow position
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Container(
                          height: 150,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(
                                items[index].icon,
                              ),
                            ),
                          ),
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(
                                        20,
                                      ),
                                    ),
                                    height: 30,
                                    width: 59,
                                    child: Center(
                                      child: RichText(
                                        text: TextSpan(
                                          text: '\$ ',
                                          style: TextStyle(
                                              color: Enviroment.primeryColor),
                                          children: [
                                            TextSpan(
                                              text: items[index].price,
                                              style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontSize: 18,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: index == 1
                                          ? Enviroment.primeryColor
                                          : Colors.white30,
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    width: 35,
                                    height: 35,
                                    child: const Icon(
                                      Icons.favorite,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 15,
                            ),
                            Text(
                              items[index].title,
                              style: const TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 15,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              items[index].subTitle,
                              style: const TextStyle(
                                color: Color(0xFF7E8392),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    top: 127,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 30,
                        width: 59,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Padding(
                          padding: const EdgeInsets.all(6),
                          child: Row(
                            children: [
                              Text(
                                items[index].raiting.toString(),
                                style: const TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 12,
                                ),
                              ),
                              const Icon(
                                Icons.star,
                                color: Colors.yellow,
                                size: 13,
                              ),
                              const Text(
                                '(25+)',
                                style: TextStyle(
                                    color: Color(0xFF7E8392),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 7),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
