import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:nike_shoes/config/constants/environament.dart';
import 'package:nike_shoes/presentation/widgets/widgets.dart';
import 'package:nike_shoes/presentation/providers/providers.dart';

class CustomAppBar extends ConsumerStatefulWidget {
  const CustomAppBar({super.key});

  @override
  CustomAppBarState createState() => CustomAppBarState();
}

class CustomAppBarState extends ConsumerState<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: SizedBox(
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: IconButton(
                iconSize: 40,
                onPressed: () {
                  final _state = ref.watch(sideMenuProvider).currentState;
                  if (_state!.isOpened)
                    _state.closeSideMenu(); // close side menu
                  else
                    _state.openSideMenu();
                },
                icon: const CustomButton(img: 'assets/img/menu.png'),
              ),
            ),
            Column(
              children: [
                Row(
                  children: const [
                    Text(
                      'Deliver to ',
                      style: TextStyle(
                        color: Color(0xFF8C9099),
                        fontWeight: FontWeight.w700,
                        fontSize: 17,
                      ),
                    ),
                    Icon(
                      Icons.arrow_drop_down,
                      color: Color(0xFF8C9099),
                    )
                  ],
                ),
                Text(
                  '4102 Pretty View Lane',
                  style: TextStyle(
                    color: Enviroment.primeryColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  width: 45,
                  'assets/img/avatar2.png',
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
