import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:nike_shoes/config/constants/environament.dart';

class CustomBottomNavigation extends StatelessWidget {
  final int currentIndex;

  const CustomBottomNavigation({super.key, required this.currentIndex});

  void onItemTapped(BuildContext context, int index) {
    // context.go('');
    switch (index) {
      case 0:
        context.go('/home/0');
        break;

      case 1:
        context.go('/home/1');
        break;

      case 2:
        context.go('/home/2');
        break;

      case 3:
        context.go('/home/3');
        break;

      case 4:
        context.go('/home/4');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (value) => onItemTapped(context, value),
        elevation: 0,
        selectedItemColor: Enviroment.primeryColor,
        unselectedItemColor: Colors.green,
        showSelectedLabels: false, // <-- HERE
        showUnselectedLabels: false, //
        items: [
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/svg/xplore.svg',
              ),
              label: ''),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/svg/point.svg',
              ),
              label: 'Populares'),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/svg/shop.svg',
              ),
              label: 'Favoritos'),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/svg/favo.svg',
              ),
              label: 'Favoritos'),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/svg/ring.svg',
              ),
              label: 'Favoritos'),
        ]);
  }
}
