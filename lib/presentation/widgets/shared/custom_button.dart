import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    super.key,
    required this.img,
    this.width = 40,
    this.height = 40,
  });

  final String img;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 4,
            offset: Offset(4, 5), //hadow position
          ),
        ],
      ),
      width: width,
      height: height,
      child: Image.asset(img),
    );
  }
}
