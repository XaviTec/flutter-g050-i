import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:nike_shoes/presentation/providers/providers.dart';
import 'package:nike_shoes/presentation/views/views.dart';
import 'package:nike_shoes/presentation/widgets/widgets.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key, required this.pageIndex});
  static const name = 'home-screen';
  final int pageIndex;

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends ConsumerState<HomeScreen>
    with AutomaticKeepAliveClientMixin {
  late PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController(keepPage: true);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  final viewRoutes = const [
    HomeView(),
    Placeholder(),
    CartView(),
    Placeholder(),
    Placeholder(),
  ];

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (pageController.hasClients) {
      pageController.animateToPage(
        widget.pageIndex,
        curve: Curves.easeInOut,
        duration: const Duration(milliseconds: 250),
      );
    }
    return SideMenu(
      key: ref.watch(sideMenuProvider),
      maxMenuWidth: 332,
      type: SideMenuType.shrinkNSlide,
      background: const Color(0xFFFFFFFF),
      menu: const MenuSide(),
      child: Scaffold(
        body: PageView(
          //* Esto evitará que rebote
          physics: const NeverScrollableScrollPhysics(),
          controller: pageController,
          // index: pageIndex,
          children: viewRoutes,
        ),
        bottomNavigationBar: CustomBottomNavigation(
          currentIndex: widget.pageIndex,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
