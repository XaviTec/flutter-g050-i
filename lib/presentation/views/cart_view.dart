import 'package:flutter/material.dart';
import 'package:nike_shoes/config/constants/environament.dart';
import 'package:nike_shoes/infraestructure/models/detalle_factura.dart';
import 'package:nike_shoes/infraestructure/models/models.dart';
import 'package:nike_shoes/presentation/widgets/widgets.dart';

class CartView extends StatelessWidget {
  const CartView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _AppBar(),
          _ItemCard(),
          const SizedBox(
            height: 30,
          ),
          _PromoCode(),
          SizedBox(
            height: 350,
            child: ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              itemCount: detalleFactura.length,
              itemBuilder: (context, index) => ListTile(
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                title: Text(
                  detalleFactura[index].title,
                  style: const TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                trailing: RichText(
                  text: TextSpan(
                    text: detalleFactura[index].price,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                    children: const [
                      TextSpan(
                        text: ' USD',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              separatorBuilder: (BuildContext context, int index) {
                return const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 18),
                  child: Divider(
                    color: Colors.grey,
                  ),
                );
              },
            ),
          ),
          const _Buttom(name: 'CHECKOUT', width: 248, height: 57)
        ],
      ),
    );
  }
}

class _PromoCode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 385,
      height: 60,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          color: const Color(
            0xFFEEEEEE,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              'Promo Code',
              style: TextStyle(color: Color(0xFFBEBEBE)),
            ),
          ),
          _Buttom(name: 'Apply', width: 96, height: 44)
        ],
      ),
    );
  }
}

class _Buttom extends StatelessWidget {
  const _Buttom({
    required this.name,
    required this.width,
    required this.height,
  });
  final String name;
  final double width;
  final double height;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Enviroment.primeryColor,
          borderRadius: BorderRadius.circular(28)),
      width: width,
      height: height,
      child: Center(
          child: Text(
        name,
        style:
            const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      )),
    );
  }
}

class _AppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50, right: 20, left: 20, bottom: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const CustomButton(img: 'assets/img/back.png'),
          const Text(
            'Cart',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          Container(
            width: 40,
          )
        ],
      ),
    );
  }
}

class _ItemCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        padding: EdgeInsets.zero,
        itemCount: cart.length,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: SizedBox(
            width: 385,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Image.asset(cart[index].icon),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      cart[index].title,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      cart[index].subTitle,
                      style: const TextStyle(color: Color(0xFF8C8A9D)),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      cart[index].price,
                      style: TextStyle(
                          color: Enviroment.primeryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Image.asset('assets/img/close.png'),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.remove_circle_outline,
                          color: Enviroment.primeryColor,
                          size: 30,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text(
                          '02',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.add_circle,
                          color: Enviroment.primeryColor,
                          size: 30,
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
