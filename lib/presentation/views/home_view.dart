import 'package:flutter/material.dart';
import 'package:nike_shoes/config/constants/environament.dart';
import 'package:nike_shoes/infraestructure/models/models.dart';
import 'package:nike_shoes/presentation/widgets/widgets.dart';

class HomeView extends StatefulWidget {
  const HomeView({
    super.key,
  });
  static const name = 'home-view';

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return CustomScrollView(
      slivers: [
        const SliverAppBar(
          floating: true,
          backgroundColor: Colors.white,
          flexibleSpace: FlexibleSpaceBar(
            titlePadding: EdgeInsets.zero,
            title: CustomAppBar(),
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            childCount: 1,
            (context, index) {
              return Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  _Title(),
                  const Search(),
                  Category(menu: menu),
                  _Featured(),
                  ListRestaurants(restaurants: restaurants),
                  _Popular(),
                  PopularItems(items: items)
                ],
              );
            },
          ),
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _Title extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Text(
      'What would you like \nto order',
      style: TextStyle(
        color: Color(0xFF323643),
        fontWeight: FontWeight.w900,
        fontSize: 37,
      ),
    );
  }
}

class _Featured extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: const Text(
        'Featured Restaurants',
        style: TextStyle(
          color: Color(0xFF323643),
          fontWeight: FontWeight.w900,
          fontSize: 21,
        ),
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'View All',
            style: TextStyle(
              color: Enviroment.primeryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          Icon(
            Icons.keyboard_arrow_right,
            color: Enviroment.primeryColor,
            size: 17,
          ),
        ],
      ),
    );
  }
}

class _Popular extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const ListTile(
      title: Text(
        'Popular Items',
        style: TextStyle(
          color: Color(0xFF323643),
          fontWeight: FontWeight.w900,
          fontSize: 21,
        ),
      ),
    );
  }
}
